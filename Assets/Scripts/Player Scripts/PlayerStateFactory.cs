using System.Collections;
using System.Collections.Generic;

public class PlayerStateFactory
{
    enum PlayerStates{
        idle,
        walk,
        run,
        grounded,
        jump,
        falling,
        slam
    }
    Player _context;
    Dictionary<PlayerStates, PlayerBaseState> _states = new Dictionary<PlayerStates, PlayerBaseState>();
    public PlayerStateFactory(Player currentContext){
        this._context = currentContext;
        _states[PlayerStates.idle] = new PlayerIdleState(_context, this);
        _states[PlayerStates.walk] = new PlayerWalkingState(_context, this);
        _states[PlayerStates.run] = new PlayerRunningState(_context, this);
        _states[PlayerStates.grounded] = new PlayerGroundedState(_context, this);
        _states[PlayerStates.jump] = new PlayerJumpingState(_context, this);
        _states[PlayerStates.falling] = new PlayerFallingState(_context, this);
        _states[PlayerStates.slam] = new PlayerSlamState(_context, this);
    }
    public PlayerBaseState Idle(){
        return _states[PlayerStates.idle];
    }
    public PlayerBaseState Walk(){
        return _states[PlayerStates.walk];
    }
    public PlayerBaseState Run(){
        return _states[PlayerStates.run];
    }
    public PlayerBaseState Jump(){
        return _states[PlayerStates.jump];
    }
    public PlayerBaseState Grounded(){
        return _states[PlayerStates.grounded];
    }
    public PlayerBaseState Falling(){
        return _states[PlayerStates.falling];
    }
    public PlayerBaseState Slam(){
        return _states[PlayerStates.slam];
    }
}
