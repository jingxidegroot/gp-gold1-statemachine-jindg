
public interface IRootState
{
    void UpdateLastHeight();
    void SetupMovementMultipliers();
}
