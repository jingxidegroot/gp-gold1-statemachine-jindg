using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]

public class Player : MonoBehaviour
{
    private GameObject _playerGameObject;
    private Transform _playerTransform;
    private Rigidbody2D _playerRigidBody;
    private PlayerBaseState _currentState;
    private PlayerControls _playerControls;
    private SpriteRenderer _playerSpriteRenderer;
    private Color _colorWhenSlamming = new Color(1,0,0,1);
    private Color _defaultColor = new Color(1,1,1,1);
    private Vector2 _currentMovementInput;
    private Vector3 _currentMovement;
    private float _lastHeight;
    private bool _isMovementPressed;
    private bool _isJumpPressed;
    private bool _isRunPressed;
    private bool _isSlamPressed;
    private bool _isGrounded = true;
    private bool _needJumpPress = true;
    private bool _playerIsSlamming;
    private string _groundTag = "Ground";
    [SerializeField] private float _jumpforce = 300f;
    [SerializeField] private float _slamForce = 1000f;
    [SerializeField] private float _walkingSpeed = 100f;
    [SerializeField] private float _runningMultiplier = 3f;
    [SerializeField] private float _gravityWhenJumping = 0.5f;
    [SerializeField] private float _gravityWhenFalling = 2f;
    [SerializeField] private float _gravityWhenGrounded = 1f;
    [SerializeField] private float _movementMultiplierWhenGrounded = 1f;
    [SerializeField] private float _movementMultiplierWhenJumping = 0.5f;
    [SerializeField] private float _movementMultiplierWhenFalling = 0.3f;
    private float _currentMovementMultiplier;
    private PlayerStateFactory _states;
    public Rigidbody2D PlayerRigidBody{get{return _playerRigidBody;}}
    public PlayerStateFactory States{get{return _states;}set{_states = value;}}
    public PlayerBaseState CurrentState{get{return _currentState;}set{_currentState = value;}}
    public Transform PlayerTransform{get{return _playerTransform;}}
    public SpriteRenderer PlayerSpriteRenderer{get{return _playerSpriteRenderer;}}
    public Color ColorWhenSlamming{get{return _colorWhenSlamming;}}
    public Color DefaultColor{get{return _defaultColor;}}
    public bool IsMovementPressed{get{return _isMovementPressed;}}
    public bool PlayerIsSlamming{get{return _playerIsSlamming;} set{_playerIsSlamming = value;}}
    public bool IsRunPressed{get{return _isRunPressed;}}
    public bool IsJumpPressed{get{return _isJumpPressed;}}
    public bool IsSlamPressed{get{return _isSlamPressed;}}
    public bool IsGrounded{get{return _isGrounded;}}
    public bool NeedJumpPress{get{return _needJumpPress;} set{_needJumpPress = value;}}
    public Vector3 CurrentMovement{get{return _currentMovement;}}
    public float LastHeight{get{return _lastHeight;} set{_lastHeight = value;}}
    public float JumpForce{get{return _jumpforce;}}
    public float SlamForce{get{return _slamForce;}}
    public float WalkingSpeed{get{return _walkingSpeed;}}
    public float RunningMultiplier{get{return _runningMultiplier;}}
    public float GravityWhenJumping{get{return _gravityWhenJumping;}}
    public float GravityWhenFalling{get{return _gravityWhenFalling;}}
    public float GravityWhenGrounded{get{return _gravityWhenGrounded;}}
    public float MovementMultiplierWhenFalling{get{return _movementMultiplierWhenFalling;}}
    public float MovementMultiplierWhenGrounded{get{return _movementMultiplierWhenGrounded;}}
    public float MovementMultiplierWhenJumping{get{return _movementMultiplierWhenJumping;}}
    public float CurrentMovementMultiplier{get{return _currentMovementMultiplier;} set{_currentMovementMultiplier = value;}}
    void Awake(){

        _playerControls = new PlayerControls();
        SetupPlayerControlCallback();
        _states = new PlayerStateFactory(this);
        _playerGameObject = GameObject.Find("Player");
        _playerRigidBody = _playerGameObject.GetComponent<Rigidbody2D>();
        _playerTransform = _playerGameObject.transform;
        _playerSpriteRenderer = _playerGameObject.GetComponent<SpriteRenderer>();
        _lastHeight = _playerTransform.position.y;
        _currentState = _states.Grounded();
        _currentState.EnterState();
    }
    private void SetupPlayerControlCallback(){
        _playerControls.CharacterControls.Move.started += OnMovementInput;
        _playerControls.CharacterControls.Move.canceled += OnMovementInput;
        _playerControls.CharacterControls.Move.performed += OnMovementInput;
        _playerControls.CharacterControls.Run.started += OnRun;
        _playerControls.CharacterControls.Run.canceled += OnRun;
        _playerControls.CharacterControls.Jump.started += OnJump;
        _playerControls.CharacterControls.Jump.canceled += OnJump;
        _playerControls.CharacterControls.Slam.started += OnSlam;
        _playerControls.CharacterControls.Slam.canceled += OnSlam;
    }
    private void OnMovementInput(InputAction.CallbackContext context){
            _currentMovementInput = context.ReadValue<Vector2>();
            _currentMovement.x = _currentMovementInput.x;
            _currentMovement.z = _currentMovementInput.y;
            _isMovementPressed =_currentMovementInput.x != 0 || +_currentMovementInput.y != 0;
    }
    private void OnRun(InputAction.CallbackContext context){
        _isRunPressed = context.ReadValueAsButton();
    }
    private void OnJump(InputAction.CallbackContext context){
        _isJumpPressed = context.ReadValueAsButton();
    }
    private void OnSlam(InputAction.CallbackContext context){
        _isSlamPressed = context.ReadValueAsButton();
    }
    void Start()
    {
    }
    void OnEnable(){
        _playerControls.CharacterControls.Enable();
    }
    void OnDisable(){
        _playerControls.CharacterControls.Disable();
    }
    void Update()
    {
        _currentState.UpdateStates();
    }
    void FixedUpdate(){
        _currentState.FixedUpdateStates();
    }
    void OnCollisionEnter2D(Collision2D collision){
        if(collision.contacts.Length > 0)
     {
         ContactPoint2D contact = collision.contacts[0];
         if(Vector2.Dot(contact.normal, Vector2.up) > 0.5)
         {
             _isGrounded = true;
         }
     }
    }
    void OnCollisionExit2D(Collision2D collision){
        if(collision.gameObject.tag.Equals(_groundTag)){
            _isGrounded = false;
        }
    }
}
