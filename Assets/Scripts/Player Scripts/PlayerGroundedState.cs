using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGroundedState : PlayerBaseState, IRootState
{
    public PlayerGroundedState(Player currentContext, PlayerStateFactory playerStateFactory)
    :base (currentContext,playerStateFactory){
        IsRootState = true;
    }
    public override void CheckSwitchState(){
        if(Context.IsJumpPressed){
            SwitchState(Factory.Jump());
        }
        else if(Context.transform.position.y - Context.LastHeight < 0){
            SwitchState(Factory.Falling());
        }
    }
    public override void EnterState(){
        InitializeSubState();
        SetupMovementMultipliers();
    }
    public override void ExitState(){

    }
    public override void FixedUpdateState(){

    }
    public override void InitializeSubState(){
        if(Context.PlayerIsSlamming){
            SetSubState(Factory.Slam());
        }
        else if(!Context.IsMovementPressed && !Context.IsRunPressed){
            SetSubState(Factory.Idle());
        }
        else if(Context.IsMovementPressed && !Context.IsRunPressed){
            SetSubState(Factory.Walk());
        }
        else{
            SetSubState(Factory.Run());
        }
    }
    public void UpdateLastHeight(){
        Context.LastHeight = Context.PlayerTransform.position.y;
    }
     public void SetupMovementMultipliers(){
        Context.PlayerRigidBody.gravityScale = Context.GravityWhenGrounded;
        Context.CurrentMovementMultiplier = Context.MovementMultiplierWhenGrounded;
    }
    public override void UpdateState(){
        CheckSwitchState();
        UpdateLastHeight();
    }
}
