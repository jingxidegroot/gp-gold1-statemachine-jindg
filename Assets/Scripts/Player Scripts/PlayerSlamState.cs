using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSlamState : PlayerBaseState
{
    public PlayerSlamState(Player currentContext, PlayerStateFactory playerStateFactory)
    :base (currentContext,playerStateFactory){
    }
    public override void CheckSwitchState(){
        if(Context.IsGrounded && !Context.IsMovementPressed && !Context.IsRunPressed){
            SwitchState(Factory.Idle());
        }
        else if(Context.IsGrounded && Context.IsMovementPressed && !Context.IsRunPressed){
            SwitchState(Factory.Walk());
        }
        else if(Context.IsGrounded && Context.IsMovementPressed && Context.IsRunPressed){
            SwitchState(Factory.Run());
        }
    }
    public override void EnterState(){
        Context.PlayerSpriteRenderer.color = Context.ColorWhenSlamming;
        Context.PlayerIsSlamming = true;
    }
    public override void ExitState(){
        Context.PlayerSpriteRenderer.color = Context.DefaultColor;
        Context.PlayerIsSlamming = false;
    }
    public override void FixedUpdateState(){
        Context.PlayerRigidBody.velocity = new Vector2(0, -Context.SlamForce * Time.deltaTime);
    }
    public override void InitializeSubState(){}
    public override void UpdateState(){
        CheckSwitchState();
    }
}