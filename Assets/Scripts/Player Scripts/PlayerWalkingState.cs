using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWalkingState : PlayerBaseState
{
    public PlayerWalkingState(Player currentContext, PlayerStateFactory playerStateFactory)
    :base (currentContext,playerStateFactory){}
    public override void CheckSwitchState(){
        if(Context.IsRunPressed && Context.IsMovementPressed){
            SwitchState(Factory.Run());
        }
        else if(!Context.IsRunPressed && !Context.IsMovementPressed){
            SwitchState(Factory.Idle());
        }
        else if(_currentSuperState.Equals(Factory.Jump()) && Context.IsSlamPressed){
            SwitchState(Factory.Slam());
        }
    }
    public override void EnterState(){
    }
    public override void ExitState(){
    }
    public override void InitializeSubState(){

    }
    public override void UpdateState(){
        CheckSwitchState();
    }
    public override void FixedUpdateState(){
        Context.PlayerRigidBody.velocity = new Vector2(Context.CurrentMovement.x * Context.WalkingSpeed * Time.deltaTime, Context.PlayerRigidBody.velocity.y);
    }
}
