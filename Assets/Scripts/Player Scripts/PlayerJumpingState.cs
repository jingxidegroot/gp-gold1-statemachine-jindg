using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJumpingState : PlayerBaseState, IRootState
{
    public PlayerJumpingState(Player currentContext, PlayerStateFactory playerStateFactory)
    :base (currentContext,playerStateFactory){
        IsRootState = true;
    }
    public override void CheckSwitchState(){
        if(Context.IsGrounded){
            SwitchState(Factory.Grounded());
        }
        else if(Context.transform.position.y - Context.LastHeight < 0){
            SwitchState(Factory.Falling());
        }
    }
    public override void EnterState(){
        InitializeSubState();
        SetupMovementMultipliers();
    }
    public override void ExitState(){
    }
    public override void InitializeSubState(){
        if(Context.PlayerIsSlamming){
            SetSubState(Factory.Slam());
        }
        else if(!Context.IsMovementPressed && !Context.IsRunPressed){
            SetSubState(Factory.Idle());
        }
        else if(Context.IsMovementPressed && !Context.IsRunPressed){
            SetSubState(Factory.Walk());
        }
        else{
            SetSubState(Factory.Run());
        }
    }
     public void SetupMovementMultipliers(){
        Context.PlayerRigidBody.gravityScale = Context.GravityWhenJumping;
        Context.CurrentMovementMultiplier = Context.MovementMultiplierWhenJumping;
    }
    public void UpdateLastHeight(){
        Context.LastHeight = Context.PlayerTransform.position.y;
    }
    public override void UpdateState(){
        CheckSwitchState();
        UpdateLastHeight();
    }
    public override void FixedUpdateState(){
        if(Context.NeedJumpPress){
            Context.PlayerRigidBody.velocity = new Vector2(Context.PlayerRigidBody.velocity.x, Context.JumpForce * Time.deltaTime);
            Context.NeedJumpPress = false;
        }
    }
}
