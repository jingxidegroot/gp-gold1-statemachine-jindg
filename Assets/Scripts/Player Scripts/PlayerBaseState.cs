using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerBaseState
{
    private bool _isRootState = false;
    private Player _context;
    private PlayerStateFactory _factory;
    protected PlayerBaseState _currentSubState;
    protected PlayerBaseState _currentSuperState;
    protected bool IsRootState {
        set{ _isRootState = value;}
    }
    protected Player Context{
        get{return _context;}
    }
    protected PlayerStateFactory Factory{
        get{return _factory;}
    }
    public PlayerBaseState(Player currentContext, PlayerStateFactory playerStateFactory){
        _context = currentContext;
        _factory = playerStateFactory;
    }
    public abstract void EnterState();
    public abstract void UpdateState();
    public abstract void FixedUpdateState();
    public abstract void ExitState();
    public abstract void CheckSwitchState();
    public abstract void InitializeSubState();
    public void UpdateStates(){
        UpdateState();
        if(_currentSubState != null){
            _currentSubState.UpdateStates();
        }
    }
    public void FixedUpdateStates(){
        FixedUpdateState();
        if(_currentSubState  != null){
            _currentSubState.FixedUpdateStates();
        }
    }
    protected void SwitchState(PlayerBaseState newState){
        ExitState();
        newState.EnterState();
        if(_isRootState){
            _context.CurrentState = newState;
        }
        else if(_currentSuperState != null){
            _currentSuperState.SetSubState(newState);
        }
    }
    protected void SetSuperState(PlayerBaseState newSuperState){
        _currentSuperState = newSuperState;
    }
    protected void SetSubState(PlayerBaseState newSubstate){
        _currentSubState= newSubstate;
        _currentSubState.SetSuperState(this);
    }
}
