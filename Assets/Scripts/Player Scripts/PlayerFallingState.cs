using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFallingState : PlayerBaseState, IRootState
{
    public PlayerFallingState(Player currentContext, PlayerStateFactory playerStateFactory)
    :base (currentContext,playerStateFactory){
    IsRootState = true;
    }
    public override void CheckSwitchState(){
        if(Context.IsGrounded){
            SwitchState(Factory.Grounded());
        }
        if(Context.NeedJumpPress && Context.IsJumpPressed){
            SwitchState(Factory.Jump());
        }
    }
    public override void EnterState(){
        InitializeSubState();
        SetupMovementMultipliers();
    }
    public override void ExitState(){
        Context.NeedJumpPress = true;
    }
    public override void FixedUpdateState(){}
   public override void InitializeSubState(){
        if(Context.PlayerIsSlamming){
            SetSubState(Factory.Slam());
        }
        else if(!Context.IsMovementPressed && !Context.IsRunPressed){
            SetSubState(Factory.Idle());
        }
        else if(Context.IsMovementPressed && !Context.IsRunPressed){
            SetSubState(Factory.Walk());
        }
        else{
            SetSubState(Factory.Run());
        }
    }
    public void UpdateLastHeight(){
        Context.LastHeight = Context.PlayerTransform.position.y;
    }
    public void SetupMovementMultipliers(){
        Context.PlayerRigidBody.gravityScale = Context.GravityWhenFalling;
        Context.CurrentMovementMultiplier = Context.MovementMultiplierWhenFalling;
    }
    public override void UpdateState(){
        CheckSwitchState();
        UpdateLastHeight();
    }
}
